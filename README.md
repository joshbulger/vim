
# How to clone this repo to your system
git clone --recursive git@gitlab.com:joshbulger/vim.git ~/.vim

# initialize and update git repos
git submodule update --init --recursive
git submodule foreach --recursive git pull origin master

# Adding submodules:
git submodule add ${git_repo} bundle/${git_repo}

