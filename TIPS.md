# Create terminal session
:term /bin/bash

# Split the window
:sp OR :vs for vertical

# Resize windows
:res _int_ OR Ctrl + w; [+,-] OR :vertical resize _int_ OR Ctrl + w; [>,<]

# Move between windows
Ctrl + w; [h,j,k,l]

# Registers
"${key} 

# Rotate windows
Ctrl + w; [r|R]


